Compact encoding of the leap seconds list
=========================================

The list of leap seconds is published in a number of places. Most
authoritative is [the IERS list published from the Paris
Observatory](https://hpiers.obspm.fr/eoppc/bul/bulc/Leap_Second.dat)
and most useful is [the version published by
NIST](ftp://time.nist.gov/pub/leap-seconds.list). However neither of
them are geared up for distributing leap seconds to (say) every NTP
server.

For a couple of years, I have published the leap second list in the
DNS as a set of fairly human-readable AAAA records. There's an example
in [my message to the LEAPSECS
list](https://pairlist6.pair.net/pipermail/leapsecs/2015-January/005438.html)
though I have changed the format to avoid making any of them look like
real IPv6 addresses.

[Poul-Henning Kamp also publishes leap second information in the
DNS](http://phk.freebsd.dk/time/20151122/) though he only
publishes an encoding of the most recent [Bulletin
C](https://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat) rather than
the whole history.

I have recently added a set of PHK-style A records at
`leapsecond.dotat.at` listing every leap second, plus an "illegal"
record representing the point after which the difference between TAI
and UTC is unknown. I have also added a `next.leapsecond.dotat.at`
which should be the same as PHK's `leapsecond.utcd.org`.

There are also some HINFO records that briefly explain the other
records at `leapsecond.dotat.at`.

One advantage of using the DNS is that the response can be signed and
validated using DNSSEC. One disadvantage is that big lists of
addresses rapidly get quite unwieldy. There can also be problems with
DNS messages larger than 512 bytes. For `leapsecond.dotat.at` the
answers can get a bit chunky:

<table>
<tr><td><th>plain<th>DNSSEC
<tr><th>A<td>485<td>692
<tr><th>AAAA<td>821<td>1028
</table>

Terse
-----

I've thought up a simple and brief way to list the leap seconds, which
looks like this:

    6+6+12+12+12+12+12+12+12+18+12+12+24+30+24+12+18+12+12+18+18+18+84+36+42+36+18+5?

[ABNF](https://tools.ietf.org/html/rfc5234) syntax:

        leaps  =  *leap end
        leap   =  gap delta
        end    =  gap "?"
        delta  =  "-" / "+"
        gap    =  1*DIGIT

Each leap second is represented as a decimal number, which counts the
months since the previous leap second, followed by a "+" or a "-" to
indicate a positive or negative leap second. The sequence starts at
the beginning of 1972, when TAI-UTC was 10s.

So the first leap is "6+", meaning that 6 months after the start of
1972, a leap second increases TAI-UTC to 11s. The "84+" leap
represents the gap between the leap seconds at the end of 1998 and end
of 2005 (7 * 12 months).

The list is terminated by a "?" to indicate that the IERS have not
announced what TAI-UTC will be after that time.

Rationale
---------

[ITU recommendation
TF.460-6](http://www.itu.int/rec/R-REC-TF.460-6-200202-I/en) specifies
in paragraph 2.1 that leap seconds can happen at the end of any month,
though in practice the preference for the end of December or June has
never been overridden. Negative leap seconds are also so far only a
theoretical possibility.

So, counting months between leaps is the largest radix permitted,
giving the smallest (shortest) numbers.

I decided to use decimal and mnemonic separators to keep it simple.

Binary
------

If you want something super compact, then bit-banging is the way to do
it. Here's a binary version of the leap second list, 15 bytes
displayed as hexadecimal in network byte order.

        001111111211343 12112229D5652F4

This is a string of 4-bit nybbles, upper nybble before lower nybble of
each byte.

If the value of this nybble V is less than 0x8, it is treated as a
pair of nybbles 0x9V. This abbreviates the common case.

Otherwise, we consider this nybble Q and the following nybble V as a
pair 0xQV.

Q contains four flags,

        +-----+-----+-----+-----+
        |  W  |  M  |  N  |  P  |
        +-----+-----+-----+-----+

W is for width:

* W == 1 indicates a QV pair in the string.
* W == 0 indicates this is a bare V nybble.

M is the month multiplier:

* M == 1 indicates the nybble V is multiplied by 1
* M == 0 indicates the nybble V is multiplied by 6

NP together are NTP-compatible leap indicator bits:

* NP == 00 indicates no leap second
* NP == 01 == 1 indicates a positive leap second
* NP == 10 == 2 indicates a negative leap second
* NP == 11 == 3 indicates an unknown leap second

The latter is equivalent to the ? terminating the text version.

The event described by NP occurs a number of months after the previous
event, given by

        (M ? 1 : 6) * (V + 1).

That is, a 6 month gap can be encoded as M=1, V=5 or as M=0, V=0.

The "no leap second" option comes into play when the gap between leap
seconds is too large to fit in 4 bits. In this situation you encode a
number of "no leap second" gaps until the remaining gap fits.

The recommended way to break up long gaps is as follows. Gaps that are
a multiple of 6 months long should be encoded as a number of `16*6`
month gaps, followed by the remainder. Gaps up to 16 months can be
encoded in one QV pair. Other gaps should be rounded down to a whole
number of years and encoded as a X*6 month gap, which is followed by a
gap for the remaining few months.

To align the list to a whole number of bytes, add a redundant 9 nybble
to turn a bare V nybble into a QV pair.

In the current leap second list, every gap is encoded as a single V
nybble, except for the 84 month gap which is encoded as QV = 0x9D, and
the last 5 months encoded as QV = 0xF4.

Publication
-----------

There is now a TXT record at `leapsecond.dotat.at` containing the
human-readable terse form of the leap seconds list. This is gives you
a 131 byte plain DNS response, or a 338 byte DNSSEC signed response.

I've published the binary version using a private-use TYPE65432 record
which saves 67 bytes.

There is code to download and check the consistency of the leapseconds
files from the IERS, NIST, and USNO, generate the DNS records, and
update the DNS if necessary, at
<http://dotat.at/cgi/git/leapseconds.git>.

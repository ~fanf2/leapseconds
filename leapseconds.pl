#!/usr/bin/perl

use warnings;
use strict;

use File::Slurp;
use FindBin;
use List::Util qw(reduce);
use POSIX;
use URI;

my %month;
@month{qw(jan feb mar apr may jun
	  jul aug sep oct nov dec)} = (1 .. 12);

my $epoch_unix = 40587;
my $epoch_ntp = 15020;

sub date2mjd {
	use integer;
	my ($y,$m,$d) = @_;
        if ($m > 2) { $m += 1; } else { $m += 13; $y -= 1; }
	return $y*1461/4 - $y/100 + $y/400 + $m*153/5 + $d - 679004;
}

sub hash2mjd {
	my $h = shift;
	return undef unless $h->{year} && $h->{month} && $h->{day};
	$h->{month} = $month{lc $1}
	    if $h->{month} =~ m{^([A-Za-z]{3})};
	return date2mjd $h->{year}, $h->{month}, $h->{day};
}

sub hash2date {
	my $h = shift;
	return sprintf "%d-%02d-%02d",
	     $h->{year}, $h->{month}, $h->{day};
}

sub hash2months {
	my $h = shift;
	return ($h->{year} - 1972) * 12 + $h->{month}
	    if $h->{year} && $h->{month};
}

sub seconds2mjd {
	my ($epoch,$secs) = @_;
	return undef unless $secs % 86400 == 0;
	return $secs / 86400 + $epoch;
}

sub mjd2seconds {
	my ($epoch,$mjd) = @_;
	return 86400 * ($mjd - $epoch);
}

sub  ntp2mjd { return seconds2mjd $epoch_ntp,  @_; }
sub unix2mjd { return seconds2mjd $epoch_unix, @_; }
sub mjd2ntp  { return mjd2seconds $epoch_ntp,  @_; }
sub mjd2unix { return mjd2seconds $epoch_unix, @_; }

sub leap {
	my $l = $_[0]->{leap} // 2;
	return $_[$l + 2];
}
sub peal {
	my $l = index $_[0], $_[1];
	return $l < 0 ? undef : $l - 1;
}

sub encode_txt {
	return join '', map {
		sprintf "%d%s", $_->{gap},
		    leap $_, qw( - . + ? );
	} @_[1..$#_];
}

sub decode_txt {
	use integer;
	my $txt = shift;
	my @l = map { m{(\d+)(.)};
		      { gap => $1, leap => peal "-.+", $2 }
		    } $txt =~ m{\d+.}g;
	unshift @l, { tai_utc => 10, leap => 0, tot => 0 };
	reduce { $b->{tot} = $a->{tot} + $b->{gap};
		 $b->{tai_utc} = $a->{tai_utc} + $b->{leap}
		     if defined $b->{leap};
		 $b } @l;
	splice @l, 1, $#l-1, grep { $_->{leap} } @l[1..$#l-1];
	reduce { $b->{gap} = $b->{tot} - $a->{tot};
		 $b } @l;
	return map { $_->{year} = $_->{tot} / 12 + 1972;
		     $_->{month} = $_->{tot} % 12 + 1;
		     $_->{day} = 1;
		     $_->{mjd} = hash2mjd $_;
		     $_ } @l;
}

sub split6 {
	my $n = ($_[0] - $_[1]) / 6;
	return "16x" x ($n / 16) .
	    $n % 16 . "x" .
	    $_[1] . "m";
}

sub txt2hex {
	my $spread = $_[0] =~ s{(\d+)}{
		$1 % 6 == 0 ? split6 $1, 0 :
		    $1 > 16 ? split6 $1, $1 % 12 : "$1m"
	    }egr;
	$spread =~ s{((\d+)\w)}{$2 ? $1 : ""}eg;
	my %LI = ( '-' => 0x20, '' => 0x00, '+' => 0x10, '?' => 0x30 );
	my %MM = ( m => 0x40, x => 0x00 );
	my $hex = unpack 'H*', pack 'C*',
	    map { m{(\d+)(\w)(.?)};
		  $1 - 1 + $MM{$2} + $LI{$3} + 0x80 }
	    $spread =~ m{\d+\w[-+?]?}g;
	$hex =~ s{(.)(.)}{hex $1 == 9 && hex $2 < 8 ? $2 : $1.$2}eg;
	$hex =~ s{((?:..)*?)([0-7])}{${1}9${2}} if 1 & length $hex;
	return uc $hex;
}

sub hex2txt {
	return join '', map {
		m{^([89A-Fa-f])?(.)$};
		my $m = hex($2) + 1;
		my $c = hex($1 // 9);
		$m *= 6 unless $c & 4;
		$c = substr ".+-?", $c & 3, 1;
		"$m$c";
	} $_[0] =~ m{[0-7]|[89A-Fa-f][0-9A-Fa-f]}g;
}

sub generate_iers {
	return map { sprintf "%11.1f   %2d %2d %4d   %6d\n",
			 @$_{qw{mjd day month year tai_utc}}
		 } @_[0..$#_-1];
}

if (@ARGV == 1 and $ARGV[0] eq '-txt') {
	my $txt = qx{dig +short leapsecond.dotat.at. in txt};
	$txt =~ s{^"((?:\d+[-+])+\d+[?])"\s*$}{$1}
	    or die "could not dig leapsecond.dotat.at txt\n";
	print generate_iers decode_txt $txt;
	exit;
}
if (@ARGV == 1 and $ARGV[0] eq '-bin') {
	my $hex = qx{dig +short leapsecond.dotat.at. in type65432};
	$hex =~ s{^\\#\s+\d+\s+([0-9A-Fa-f]+)\s*$}{$1}
	    or die "could not dig leapsecond.dotat.at type65432\n";
	print generate_iers decode_txt hex2txt $hex;
	exit;
}

sub equal {
	return defined reduce { $a eq $b ? $a : undef } @_;
}

sub hmap (&%) {
	my ($fun,%hash) = @_;
	my @keys = keys %hash;
	@hash{@keys} = map &$fun, @hash{@keys};
	return %hash;
}

sub verify {
	my ($description,%map) = @_;
	die "mismatched $description\n",
	    map { sprintf "%s %s\n", $map{$_}, $_ } keys %map
	    unless equal values %map;
}

sub GET_cached {
	my $file = shift;
	my $url = shift;
	return read_file $file
	    if -f $file and 1 > -M $file;
	my $r = qx{curl -Ssf $url};
	if ($?) {
		die sprintf "failed to get %s\n", $url;
	}
	write_file $file, $r;
	return $r;
}

sub check_line {
	my $name = shift,
	my $h = shift;
	die "could not parse $name line $_\n"
	    unless $h;
	die "mismatched dates in $name line $_\n"
	    unless $h->{mjd} and
		   $h->{mjd} == hash2mjd $h;
	return $h;
}

sub GET_parse {
	return unless @_;
	my $url = shift;
	my $fun = shift;

	my $name = URI->new($url)->host();
	my $file = GET_cached "$FindBin::Bin/.cache.$name", $url;

	$file =~ m{\n\#\s+File expires on[:\s]+(\d+)\s+(\w+)\s+(\d+)\s*\n};
	my $exp = { day => $1, month => $2, year => $3 };
	$exp->{mjd} = hash2mjd $exp;
	die "expired file from $name\n"
	    if $exp->{mjd} and time > mjd2unix $exp->{mjd};

	$file =~ s{^(#[^\n]*\n)*}{};
	$file =~ s{\n(#[^\n]*\n)*$}{\n};

	return $name, $exp,
	    map check_line($name,&$fun), split /\n/, $file;
}

my %source = (
    'https://hpiers.obspm.fr/eoppc/bul/bulc/Leap_Second.dat'
	=> sub {
	    m{^\s*(\d+)\.0
	      \s+(\d+)\s+(\d+)\s+(\d+)
	      \s+(\d+)
	      \s*$}x
	    and {
		mjd	=> $1,
		day	=> $2,
		month	=> $3,
		year	=> $4,
		tai_utc	=> $5,
	    }
	},
    'ftp://ftp.nist.gov/pub/time/leap-seconds.list'
	=> sub {
	    m{^\s*(\d+)\s+(\d+)\s+[#]
	      \s+(\d+)\s+(\w+)\s+(\d+)
	      \s*$}x
	    and {
		mjd	=> ntp2mjd($1),
		tai_utc	=> $2,
		day	=> $3,
		month	=> $4,
		year	=> $5,
	    }
	},
    'http://maia.usno.navy.mil/ser7/leapsec.dat',
	=> sub {
	    m{^\s*(\d+)\s+(\w+)\s+(\d+)
	      \s+=JD\s+(\d+)\.5
	      \s+TAI-UTC=\s+(\d+)\.0
	      \s+S\s+.*$}x
	    and {
		year	=> $1,
		month	=> $2,
		day	=> $3,
		mjd	=> $4 - 2400000,
		tai_utc	=> $5,
	    }
	},
    );

my %exp;
my %leap;
while (my ($name, $exp, @leap) = GET_parse each %source) {
	$exp{$name} = $exp if $exp->{mjd};
	$leap{$name} = \@leap;
}

verify "expiry times",
    hmap { hash2date($_) } %exp;

verify "leap second counts",
    hmap { scalar @$_ } %leap;

my @leap = @{ $leap{each %leap} };
for my $i (0 .. $#leap) {
	verify "leap second $i",
	    hmap { sprintf "%s %d",
		   hash2date($_->[$i]),
		   $_->[$i]->{tai_utc} } %leap;
}
reduce { $b->{leap} = $b->{tai_utc} - $a->{tai_utc};
	 $b } @leap;
push @leap, $exp{each %exp};
$leap[-1]->{tai_utc} = $leap[-2]->{tai_utc};
reduce { $b->{gap} = hash2months($b) - hash2months($a);
	 $b } @leap;

# fuzz test
for (1..100) {
	my $txt = join '', map {
		sprintf "%d%s",
		    1 + int rand 100,
		    substr "+-", int rand 2, 1
	    } 1 .. int rand 20;
	$txt =~ s{.$}{?};
	my $ed = encode_txt decode_txt $txt;
	die "bad txt\n$txt\n$ed\n" unless $txt eq $ed;
	my $hex = txt2hex $txt;
	my $th = encode_txt decode_txt hex2txt $hex;
	die "bad hex\n$txt\n$hex\n$th\n" unless $txt eq $th;
}

sub crc8 {
	my $crc = shift;
	my $len = shift;
	$crc = 0x54a9abf8 ^ ($crc << (32 - $len));
	for my $i (1 .. $len) {
		$crc ^= (0x12f << 23)
		    if $crc & (1 << 31);
		$crc <<= 1;
	}
	return ($crc >> 24) & 0xff;
}

sub leap_a {
	my $leap = shift;
	my $w = hash2months $leap;
	$w = ($w << 2) | leap $leap, 1, 0, 2, 3;
	$w = ($w << 7) | $leap->{tai_utc} - ($leap->{leap} // 0);
	$w = ($w << 8) | crc8($w, 20);
	$w |= 0xf << 28;
	return join '.', unpack 'C*', pack 'N', $w;
}

sub leap_aaaa {
	my $leap = shift;
	my $t = mjd2unix $leap->{mjd};
	my $minute = strftime '::%Y:%m:%d:%H:%M', gmtime $t - 1;
	$minute =~ s{:0+}{:}g;
	return sprintf '%s:%s', $minute, leap $leap, 58, 59, 60, 59;
}

my %type = ( A => \&leap_a, AAAA => \&leap_aaaa );

sub dig {
	my $answer = qx{dig +norec +noall +nottl +answer @_};
	die "DNS lookup failed for @_\n" unless $answer;
	$answer =~ s{[ \t]+}{ }g;
	return split /\n/, $answer;
}

my $update;
sub update {
	my $verb = shift;
	return unless @_;
	$update |= print "ttl 3600\n" unless $update;
	print map "$verb $_\n", @_;
}

sub nsdiff {
	my $owner = shift;
	my $type = shift;
	my @old = dig $owner, $type;
	my @new = map "$owner IN $type $_", @_;
	my %old; @old{@old} = (); delete @old{@new};
	my %new; @new{@new} = (); delete @new{@old};
	update 'del', sort keys %old;
	update 'add', sort keys %new;
}
sub dns_hex {
	my $hex = shift;
	return sprintf '\# %d %s',
	    (length $hex) / 2,
	    $hex =~ s{(.{56})}{$1 }gr;
}

my $short = encode_txt @leap;
my $owner = 'leapsecond.dotat.at.';
while (my ($type,$fun) = each %type) {
	nsdiff $owner, $type, map $fun->($leap[$_]), 1 .. $#leap;
}
nsdiff $owner, 'HINFO',
    '"AAAA" "The date and time of the last second in months that end with a leap second, plus the last second of the known validity period if that is not a leap second"',
    '"A" "PHK-style encoding of leap second list"',
    '"TXT" "The intervals between leap seconds in months, separated by a + or - for positive or negative leap seconds, and terminated by a ?"',
    '"TYPE65432" "Compressed binary encoding of the TXT record"',
    '"URI" "link to longer description"';
nsdiff $owner, 'URI', '0 0 "http://dotat.at/cgi/git/leapseconds.git/blob/HEAD:/syntax.md"';
nsdiff $owner, 'TXT', "\"$short\"";
nsdiff $owner, 'TYPE65432', dns_hex txt2hex $short;
nsdiff "next.$owner", 'A', leap_a $leap[-2];
nsdiff "phk.$owner", 'CNAME', 'leapsecond.utcd.org.';
print "show\nsend\nanswer\n" if $update;
